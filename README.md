# Library Application
This project provides backend API

# DB CONFIGURATION
After running the application pls navigate to http://localhost:8080/h2-console

DriverClassName: org.h2.Driver
JDBC URL: jdbc:h2:file:~/test2
- UserName: username
- Password: password

# Swagger
You can check API requests via Swagger. Pls open below link.

http://localhost:8080/swagger-ui.html

Here are credentials to access to endpoints:
- username: user (for Library Employee) & admin (for admin)
- password: user (for Library Employee) & admin (for admin)

# Test task
**Nõuded koodile**:
- Java 11+
- Kõik tegevused peavad olema logitud. (tegevuse algatamine ja tulemus)
- REST api päringud peavad olema autenditud ja autoriseeritud.
---
**Soovitused koodile**:
- Kasutada raamistikku Spring boot või Micronaut.
- Kood peab olema kaetud testidega.
---

**Valdkond**:
- Raamatukogu
---
**Kasutajad**:
- Anonüümne kasutaja
- Raamatukogu töötaja (teenindaja)
- Raamatukogu töötaja (administraator)
---

**Anonüümne kasutaja**:
- Peab saama näha raamatukogu üldist seisu.
- Üldine seis peaks sisaldama infot nagu millised raamatud on raamatukogus, palju
eksemplare alles on, kui pikaks ajaks neid laenutada saab ning kus need raamatud
raamatukogus asuvad.
---

**Raamatukogu töötaja**:
- Peab saama näha raportit üle aja läinud laenutajatest. Raport peab sisaldama nii
laenutaja nime, raamatut, mille tagastamise aeg on üle läinud ja ka aega palju on üle läinud.
---

**Raamatukogu töötaja (teenindaja)**:
- Peab saama raamatut otsida.
- Peab saama raamatut välja laenutada.
- Peab saama raamatut vastu võtta.
- Peab saama laenutajaid lisada.
- Peab saama laenutajaid otsida.
---

**Raamatukogu töötaja (administraator)**:
- Peab saama laenutajaid lisada.
- Peab saama laenutajaid otsida.
- Peab saama raamatut otsida.
- Peab saama raamatut lisada.
- Peab saama raamatut kustutada.             
---
**Ülesande kirjeldus
Luua raamatukogu rakendus. Rakenduse näol on tegemist REST api backend rakendusega.
Rakendus peab lubama hallata nii laenutajaid, kui ka raamatuid.
Avalikult peab olema kättesaadav info kõikide raamatukogu raamatute kohta.
Tavapärane laenutuse tähtaeg on neli nädalat. Kui raamat on alla kolme kuu vana, siis laenutuse
tähtaeg on üks nädal. Kui raamatu eksemplare on järgi alla viie, siis laenutuse tähtaeg on üks
nädal.
 Antud tähtajad peaksid olema rakenduses lihtsasti seadistatavad.
