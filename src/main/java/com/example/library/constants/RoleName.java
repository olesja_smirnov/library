package com.example.library.constants;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum RoleName {

    ADMIN,
    @JsonEnumDefaultValue
    ANONYMOUS,
    EMPLOYEE;

    public interface Codes {
        String ADMIN = "ADMIN";
        String ANONYMOUS = "ANONYMOUS";
        String EMPLOYEE = "EMPLOYEE";
    }
}