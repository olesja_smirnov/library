package com.example.library.constants;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public enum Status {

	@JsonEnumDefaultValue
	AVAILABLE,
	BOOKED,
	RENTED

}