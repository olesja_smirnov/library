package com.example.library.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.lang.Nullable;
import com.example.library.constants.Status;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class BookDto {

    @Nullable
    private String title;

    @Nullable
    private String author;

    @NotNull
    private Status status;

    @Nullable
    private Long userId;

    @NotNull
    private Long quantity;

    @Nullable
    private String location;

}