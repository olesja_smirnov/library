package com.example.library.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.lang.Nullable;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class UserDto {

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private Long userIdentityNumber;

    @NotNull
    private String role;

}