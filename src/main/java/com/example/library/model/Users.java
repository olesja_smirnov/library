package com.example.library.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import org.springframework.lang.Nullable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "bigint")
    private Long id;

    // is required for registered user only
    @Column(name="first_name")
    @Nullable
    @Size(max = 60)
    private String firstName;

    // is required for registered user only
    @Column(name="last_name")
    @Nullable
    @Size(max = 60)
    private String lastName;

    // is required for registered user only
    @Column(name = "username")
    @Nullable
    private String username;

    // is required for registered user only
    @Column(name = "password")
    @Nullable
    private String password;

    // is required for anonymous user only
    @Column(name="user_identity")
    @Nullable
    private Long userIdentityNumber;

    @Column(name="role", nullable = false)
    private String role;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id", columnDefinition = "bigint"),
            inverseJoinColumns = @JoinColumn(name = "role_id", columnDefinition = "bigint"))
    private Set<Roles> roles = new HashSet<>();

    public Users(String firstName, String lastName, String username, String password, Long userIdentityNumber, String role) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userIdentityNumber = userIdentityNumber;
        this.password = password;
        this.role = role;
    }

}