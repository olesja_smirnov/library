package com.example.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.library.model.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

    Boolean existsByUsername(String username);
    Users findByUsername(String username);

}