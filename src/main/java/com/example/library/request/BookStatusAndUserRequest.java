package com.example.library.request;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookStatusAndUserRequest {

    @NotNull
    private String status;

    @NotNull
    private Long bookId;

}