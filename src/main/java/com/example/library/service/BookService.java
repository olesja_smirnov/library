package com.example.library.service;

import static java.time.ZoneOffset.UTC;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.library.constants.RoleName;
import com.example.library.constants.Status;
import com.example.library.dto.BookDto;
import com.example.library.model.Book;
import com.example.library.model.Stock;
import com.example.library.model.Users;
import com.example.library.repository.BookRepository;
import com.example.library.request.BookStatusAndUserRequest;

@Service
@Transactional
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    private Logger loggerFactory = LoggerFactory.getLogger(this.getClass().getPackage().getName());

    public List<Book> findBooks() {
        loggerFactory.info("LOG: Here is a list of all books in library");

        return bookRepository.findAll();
    }

    public Book addNewBook(BookDto newBook) {
        loggerFactory.info("LOG: new book is saved to Database");

        return bookRepository.save(Book.builder()
                .title(newBook.getTitle())
                .author(newBook.getAuthor())
                .status(Status.AVAILABLE)
                .quantity(newBook.getQuantity())
                .stock(Stock.builder().location(newBook.getLocation()).build())
                .createdAt(LocalDate.now(UTC))
                .build());
    }

    public Optional<Book> findByTitleOrAuthor(String title, String author) {
        loggerFactory.info("LOG: Book is found");

        return bookRepository.findByTitleOrAuthor(title, author);
    }

    public void deleteBook(Long id) {
        bookRepository.findById(id).ifPresent(book -> {
            loggerFactory.info("LOG: Deleting book with id " + id);
            bookRepository.delete(book);
        });
    }

    public Optional<Book> updateBookStatusAndUserDetails(BookStatusAndUserRequest request) {

        Optional<Book> book = bookRepository.findById(request.getBookId());
        if (book.isPresent()) {

            if (request.getStatus().equals(String.valueOf(Status.AVAILABLE))) {
                book.get().setStatus(Status.valueOf(request.getStatus()));
                book.get().setRentedTill(null);
                book.get().setUser(null);
                loggerFactory.info("LOG: Book is returned to library");
                bookRepository.save(book.get());
            } else {
                book.get().setStatus(Status.valueOf(request.getStatus()));
                        book.get().setUser(Users.builder()
                                .userIdentityNumber(new Random().nextLong())
                                .role(String.valueOf(RoleName.ANONYMOUS))
                                .build());
                book.get().setRentedTill(
                        (book.get().getQuantity() < 5)
                                || (book.get().getCreatedAt().plusMonths(3).isAfter(LocalDate.now(UTC))) ?
                                LocalDate.now(UTC).plusWeeks(1)
                                : LocalDate.now(UTC).plusWeeks(4)
                );
                loggerFactory.info("LOG: book is rented till " + book.get().getRentedTill());
                bookRepository.save(book.get());
            }
        }
        return book;
    }

}