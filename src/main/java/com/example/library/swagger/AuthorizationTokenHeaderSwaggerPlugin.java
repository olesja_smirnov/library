package com.example.library.swagger;

import java.util.LinkedList;
import java.util.List;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

@Component
@ConditionalOnSwaggerFeature
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER)
public class AuthorizationTokenHeaderSwaggerPlugin implements OperationBuilderPlugin {

	private static final String HEADER_DESCRIPTION = "Access token";

	private static final String NO_AUTHENTICATION_PATH = "/api/auth";

	@Override
	public void apply(OperationContext context) {

		if (context.requestMappingPattern().startsWith(NO_AUTHENTICATION_PATH)) {
			return;
		}

		List<Parameter> parameters = new LinkedList<>();
		parameters.add(new ParameterBuilder()
				.parameterType("header")
				.name("Authorization")
				.modelRef(new ModelRef("string"))
				.description(HEADER_DESCRIPTION)
				.allowMultiple(false)
				.required(true)
				.build());
		context.operationBuilder().parameters(parameters);
	}

	@Override
	public boolean supports(DocumentationType documentationType) {
		return DocumentationType.SWAGGER_2.equals(documentationType);
	}
}
