package com.example.library.web;

import static com.example.library.constants.RoleName.Codes.ADMIN;
import static com.example.library.constants.RoleName.Codes.EMPLOYEE;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.dto.BookDto;
import com.example.library.model.Book;
import com.example.library.request.BookStatusAndUserRequest;
import com.example.library.service.BookService;

@RestController
@RequestMapping(path = "/api/book")
@Api(tags = "book", value = "/api/book", description = "Operations with books")
public class BookController {

    @Autowired
    private BookService bookService;

    @ApiOperation("Get all books")
    @GetMapping
    public ResponseEntity<List<Book>> receiveBookList() {
        return ResponseEntity.ok(bookService.findBooks());
    }

    @ApiOperation("Add new book")
    @PostMapping
    @Secured({EMPLOYEE, ADMIN})
    public ResponseEntity<Book> addNewBook(@Valid @RequestBody BookDto newBook) {
        return ResponseEntity.ok(bookService.addNewBook(newBook));
    }

    @ApiOperation("Find a book")
    @GetMapping("/find")
    @Secured({EMPLOYEE, ADMIN})
    public Optional<Book> findOne(@RequestParam(required = false) String title,
                                  @RequestParam(required = false) String author) {
        return bookService.findByTitleOrAuthor(title, author);
    }

    @ApiOperation("Update book status, location and user")
    @PatchMapping("/")
    @Secured({EMPLOYEE})
    public Optional<Book> updateBookStatusAndUserDetails(@RequestBody BookStatusAndUserRequest request) {
        return bookService.updateBookStatusAndUserDetails(request);
    }

    @ApiOperation("Delete book")
    @DeleteMapping("/delete")
    @Secured({ADMIN})
    public ResponseEntity<?> deleteBook(@RequestParam Long id) {
        bookService.deleteBook(id);
        return ResponseEntity.ok().build();
    }

}