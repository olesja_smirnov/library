package com.example.library.web;

import java.util.Map;
import javax.xml.bind.ValidationException;

import com.example.library.model.Users;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.library.repository.UserRepository;
import com.example.library.service.UserService;

@RestController
@RequestMapping(path = "/api/user")
@Api(tags = "user", value = "/api/user", description = "Operations with users")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping
    public Boolean create(@RequestBody Map<String, String> body) throws ValidationException {
        String username = body.get("username");
        if (userRepository.existsByUsername(username)){

            throw new ValidationException("Username already existed");

        }

        String firstName = body.get("firstName");
        String lastName = body.get("lastName");
        String password = body.get("password");
        Long userIdentifyNumber = Long.valueOf(body.get("userIdentifyNumber"));
        String role = body.get("role");
        String encodedPassword = new BCryptPasswordEncoder().encode(password);

        userRepository.save(new Users(firstName, lastName, username, encodedPassword, userIdentifyNumber, role));
        return true;
    }

}