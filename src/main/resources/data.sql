CREATE TABLE IF NOT EXISTS users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(60) DEFAULT NULL,
  last_name VARCHAR(60) DEFAULT NULL,
  user_identity INT DEFAULT NULL,
  role VARCHAR(60) NOT NULL,
    FOREIGN KEY (role_id) REFERENCES roles(id)
);

INSERT INTO users (first_name, last_name, username, password, role) VALUES
  ('User', 'Employee', 'user', 'user', 'EMPLOYEE'),
  ('User', 'Admin', 'admin', 'admin', 'ADMIN');

CREATE TABLE IF NOT EXISTS roles (
  id INT AUTO_INCREMENT PRIMARY KEY,
  role_name VARCHAR(60) DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS stock (
  id INT AUTO_INCREMENT PRIMARY KEY,
  location VARCHAR(255) NOT NULL
);

INSERT INTO stock (location) VALUES
  ('Riiul 123');

CREATE TABLE IF NOT EXISTS book (
  id INT AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  author VARCHAR(255) NOT NULL,
  status VARCHAR(60) NOT NULL,
  user_id INT DEFAULT NULL,
  rented_till VARCHAR(60) DEFAULT NULL,
  quantity INT NOT NULL,
  stock_id VARCHAR(60) NOT NULL,
  created_at DATE NOT NULL DEFAULT CURRENT_DATE,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (stock_id) REFERENCES stock(id)
);

INSERT INTO book (title, author, status, quantity, stock_id, created_at) VALUES
  ('Some book', 'Olesja', 'AVAILABLE', 3, 1, current_date),
  ('Karupoeg Puhh', 'Alan Alexander', 'AVAILABLE', 6, 1, current_date);