package com.example.library;

import static java.time.ZoneOffset.UTC;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.*;

import com.example.library.request.BookStatusAndUserRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.library.constants.RoleName;
import com.example.library.constants.Status;
import com.example.library.dto.BookDto;
import com.example.library.model.Book;
import com.example.library.model.Stock;
import com.example.library.model.Users;
import com.example.library.service.BookService;

@RunWith(MockitoJUnitRunner.class)
public class BookServiceTest {

    private static String author = "Steven McConnell";
    private static String location = "vasak riiul esimesel real";
    private static Long quantity = 3L;
    private static LocalDate rentedTill = LocalDate.now(UTC);
    private static LocalDate rentedTillOneWeek = LocalDate.now(UTC).plusWeeks(1);
    private static String role = String.valueOf(RoleName.ANONYMOUS);
    private static Status statusAvailable = Status.AVAILABLE;
    private static Status statusRented = Status.RENTED;
    private static Stock stock = Stock.builder().location(location).build();
    private static String title = "Code Complete";
    private static Long userIdentityNumber = new Random().nextLong();

    private static Users user = Users.builder()
            .userIdentityNumber(userIdentityNumber)
            .role(role)
            .build();

    private static Book book = Book.builder()
            .title(title)
            .author(author)
            .status(statusAvailable)
            .user(user)
            .quantity(quantity)
            .stock(stock)
            .rentedTill(rentedTill)
            .build();

    private static BookDto bookDto = BookDto.builder()
            .title(title)
            .author(author)
            .status(statusAvailable)
            .userId(null)
            .quantity(quantity)
            .location(location)
            .build();

    @Mock
    private BookService bookService;

    @Test
    public void shouldFindByTitleOrAuthorTest() {

        Optional<Book> bookOptional = Optional.of(book);
        when(bookService.findByTitleOrAuthor(any(), any())).thenReturn(bookOptional);

        Optional<Book> response = bookService.findByTitleOrAuthor(title, author);
        Book bookExists = response.get();

        assertThat(response, is(notNullValue()));
        assertThat(bookExists, equalTo(book));
        assertThat(bookExists.getTitle(), equalTo("Code Complete"));
        assertThat(bookExists.getAuthor(), equalTo("Steven McConnell"));
        assertThat(bookExists.getStatus(), equalTo(statusAvailable));
        assertThat(bookExists.getQuantity(), equalTo(3L));
    }

    @Test
    public void shouldAddNewBookTest() {

        when(bookService.addNewBook(bookDto)).thenReturn(book);
        Book response = bookService.addNewBook(bookDto);

        assertThat(response, is(notNullValue()));
        assertThat(response.getAuthor(), equalTo("Steven McConnell"));
        assertThat(response.getTitle(), equalTo("Code Complete"));
        assertThat(response.getQuantity(), equalTo(3L));
        assertThat(response.getUser(), equalTo(user));
        assertThat(response.getStatus(), equalTo(statusAvailable));
        assertThat(response.getStock(), equalTo(stock));
        assertThat(response.getRentedTill(), equalTo(LocalDate.now(UTC)));
    }

    @Test
    public void shouldFindAllBooksTest() {

        List<Book> books = Collections.singletonList(book);
        when(bookService.findBooks()).thenReturn(books);

        List<Book> response = bookService.findBooks();
        Book oneBook = response.get(0);

        assertThat(response, is(notNullValue()));
        assertThat(response, hasSize(is(1)));
        assertThat(oneBook.getAuthor(), equalTo("Steven McConnell"));
        assertThat(oneBook.getTitle(), equalTo("Code Complete"));
        assertThat(oneBook.getQuantity(), equalTo(3L));
        assertThat(oneBook.getUser(), equalTo(user));
        assertThat(oneBook.getStatus(), equalTo(statusAvailable));
        assertThat(oneBook.getStock(), equalTo(stock));
        assertThat(oneBook.getRentedTill(), equalTo(LocalDate.now(UTC)));
    }

    @Test
    public void shouldUpdateBookStatusAndUserDetailsTest() {

        Long bookId = 1L;
        String statusRequest = "RENTED";

        BookStatusAndUserRequest request = BookStatusAndUserRequest.builder()
                .status(statusRequest)
                .bookId(bookId)
                .build();

        Book bookWithChangedStatus = Book.builder()
                .title(title)
                .author(author)
                .status(statusRented)
                .user(user)
                .quantity(quantity)
                .stock(stock)
                .rentedTill(rentedTillOneWeek)
                .build();

        when(bookService.updateBookStatusAndUserDetails(request)).thenReturn(Optional.of(bookWithChangedStatus));
        Optional<Book> responseOptional = bookService.updateBookStatusAndUserDetails(request);
        Book response = responseOptional.get();

        assertThat(responseOptional, is(notNullValue()));
        assertThat(response.getAuthor(), equalTo("Steven McConnell"));
        assertThat(response.getTitle(), equalTo("Code Complete"));
        assertThat(response.getQuantity(), equalTo(3L));
        assertThat(response.getUser(), equalTo(user));
        assertThat(response.getStatus(), equalTo(statusRented));
        assertThat(response.getStock(), equalTo(stock));
        assertThat(response.getRentedTill(), equalTo(LocalDate.now(UTC).plusWeeks(1)));
    }

}